import {createElement, addClass, removeClass} from "./helper.mjs";

const username = sessionStorage.getItem("username");
const createBtn = document.querySelector(".create-btn");
const roomsContainer = document.querySelector("#rooms-container");
const mainGameRooms = document.getElementById('rooms-page')
const localGameRoom = document.getElementById('game-page')

if (!username) {
    window.location.replace("/login");
}

const socket = io("", {query: {username}});


const createCarsRoom = () => {
    let sign = prompt("What's the name of room");
    const roomName = createElement({
        tagName: "div",
        className: "room-item flex-centered no-select",
        // attributes: {id: roomId}
    })
    const roomBtn = createElement({
        tagName: "button",
        className: "join",
    })
    roomName.textContent = sign.toLowerCase();
    const numOfUsersInRoom = document.createElement('p')
    numOfUsersInRoom.textContent = 'Users - 0';
    roomName.prepend(numOfUsersInRoom);

    roomBtn.textContent = 'join'
    roomName.append(roomBtn);
    roomsContainer.append(roomName);
    const joinBtns = document.querySelectorAll('.join')
    joinBtns.forEach(el => el.addEventListener('click', (e) => {
        e.preventDefault();
        changeGamePageContent()
    }))

}

const changeGamePageContent = () => {
    console.log('hello')
    mainGameRooms.classList.toggle('display-none')
    localGameRoom.classList.toggle('display-none')
}

socket.on('eventClient', function (data) {
    console.log(data);
});
socket.emit('eventServer', { data: 'Hello Server' });


if (createBtn !== null) {
    createBtn.addEventListener("click", createCarsRoom)
}


